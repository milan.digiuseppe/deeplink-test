import React, { useMemo } from 'react';
import { createStackNavigator } from '@react-navigation/stack';
import {
  getStateFromPath as getStateFromPathOrig,
  LinkingOptions,
  NavigationContainer,
  useLinkTo,
} from '@react-navigation/native';
import * as Linking from 'expo-linking';
import { View, Text, Button } from 'react-native';

const prefix = Linking.makeUrl('/');

const HomeScreen = () => {
  const linkTo = useLinkTo();
  const detailLink = Linking.makeUrl('/detail')
  return  (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <Text style={{fontSize: 16}} selectable>{detailLink}</Text>
      <Button title={'Link'} onPress={() => linkTo('/detail')} />
    </View>
  );
}

const DetailScreen = () => (
  <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
    <Text>Detail</Text>
  </View>
);

const MainStack = createStackNavigator();
const MainStackScreen: React.FC = () => {
  return (
    <MainStack.Navigator screenOptions={{headerShown: true}}>
      <MainStack.Screen
        name={'Home'}
        component={HomeScreen}
      />
      <MainStack.Screen
        name={'Detail'}
        component={DetailScreen}
      />
    </MainStack.Navigator>
  );
};

const AppStack = createStackNavigator();
const AppStackScreen: React.FC = () => {
  return (
    <AppStack.Navigator screenOptions={{ headerShown: false }}>
      <AppStack.Screen
        name={'Main'}
        component={MainStackScreen}
      />
    </AppStack.Navigator>
  );
};

const AppContainer: React.FC = () => {
  const linking = useMemo<LinkingOptions>(
    () => ({
      prefixes: [prefix],
      config: {
        screens: {
          Main: {
            screens: {
              Home: {
                path: '/home'
              },
              Detail: {
                path: '/detail'
              }
            }
          }
        }
      },
    }),
    []
  );

  return (
    <NavigationContainer linking={linking}>
      <AppStackScreen />
    </NavigationContainer>
  );
};

export default AppContainer;
